#include "logging.h"

#include <QDebug>

Logging::Logging()
{

}

Logging &Logging::get()
{
    static Logging* pMe = new Logging;
    return *pMe;
}

void Logging::logText(const char* szOrigin, long line, QString text)
{
    qDebug() << QString("%1(%2): %3").arg(szOrigin).arg(line).arg(text);
}
