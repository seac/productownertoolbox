#include "jiraissue.h"

JiraIssue::JiraIssue(int nId)
    : m_IssueType(0)
    , m_pSprint(nullptr)
{
    m_nId = nId;
}

JiraIssue::JiraIssue()
    : m_IssueType(0)
    , m_pSprint(nullptr)
{

}

QString JiraIssue::getParent() const
{
    return m_strParent;
}

void JiraIssue::setParent(const QString &Parent)
{
    m_strParent = Parent;
}

QStringList JiraIssue::getLinks() const
{
    return m_links;
}

void JiraIssue::setLinks(const QStringList &links)
{
    m_links = links;
}

long JiraIssue::getIssueType() const
{
    return m_IssueType;
}

void JiraIssue::setIssueType(const long &IssueTypeName)
{
    m_IssueType = IssueTypeName;
}

JiraSprint *JiraIssue::getSprint() const
{
    return m_pSprint;
}

void JiraIssue::setSprint(JiraSprint *Sprint)
{
    m_pSprint = Sprint;
}
