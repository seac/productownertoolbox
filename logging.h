#ifndef LOGGING_H
#define LOGGING_H

#include <QString>

class Logging
{
public:
    Logging();

    static Logging& get();

    void logText(const char* szOrigin, long line, QString text);
};

#define LOG(text...) do { Logging::get().logText(__FILE__, __LINE__, text); } while(false)

#endif // LOGGING_H
