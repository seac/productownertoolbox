#ifndef SETTINGS_H
#define SETTINGS_H

#include <QString>

#include "board.h"

class QSettings;

class Settings
{
public:
    Settings();

    static Settings& get();


    QString getUser() const;
    QString getPassword() const;
    QString getUrl() const;

    QList<Board> getBoards() const;

    void setUser(QString data);
    void setPassword(QString data);
    void setUrl(QString data);
    void setBoards(QList<Board> data);

private:
    QSettings *m_pSettings;
};

#endif // SETTINGS_H
