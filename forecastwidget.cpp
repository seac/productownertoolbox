#include "forecastwidget.h"

#include <QPaintEvent>
#include <QPainter>

#include "logging.h"

double map(double x, double in_min, double in_max, double out_min, double out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}



const Item Item::Null(Board(), QString::null, QString::null, QString::null, QString::null, 0);

class Sprint
{
public:
    Sprint(const QString &name)
        : m_strName(name)
    {

    }

    void append(const Item& item)
    {
        m_lstIssues.append(item);
    }


    QString m_strName;
    QList<Item> m_lstIssues;
};

ForecastWidget::ForecastWidget(QWidget *parent)
    : QWidget(parent)
    , m_nMaxX(std::numeric_limits<int>::lowest())
    , _pan(false)
    , scale(1)
{
    setBackgroundRole(QPalette::Dark);
    setAutoFillBackground(true);

    gradient.setStart(0,0);
    gradient.setFinalStop(200,0);
    QColor grey1(150,150,150,125);// starting color of the gradient - can play with the starting color and ,point since its not visible anyway

    // grey2 is ending color of the gradient - this is what will show up as the shadow. the last parameter is the alpha blend, its set
    // to 125 allowing a mix of th color and and the background, making more realistic shadow effect.
    QColor grey2(225,225,225,125);

    gradient.setColorAt((qreal)0, grey1 );
    gradient.setColorAt((qreal)1, grey2 );

    brushGradient = QBrush(gradient);
    _outterborderPen.setStyle(Qt::NoPen);

    linkPen.setStyle(Qt::DashLine);
    linkPen.setColor(Qt::darkRed);
    linkPen.setWidth(3);
}

void ForecastWidget::addItem(const Item &item)
{
    if (!m_items.contains(item.board))
    {
        m_items.insert(item.board, new QList<Item>);
    }

    m_items.value(item.board)->append(item);

    if (!item.belongsToBacklog())
    {
        m_boardActiveSprintSize.insert(item.board, m_items.value(item.board)->size());
    }

    int nMaxActiveSprintSize = getMaxActiveSprintSize();

    QMapIterator<Board, QList<Item>*> it(m_items);
    long lPosY = 0;
    while (it.hasNext()) {
        it.next();

        QList<Item>* pItems=it.value();
        const Board localKey = it.key();

        int nOffsetX = nMaxActiveSprintSize - m_boardActiveSprintSize.value(localKey);

        for (int i = 0; i < pItems->size(); ++i) {
            (*pItems)[i].lPosX = nOffsetX + i;
            (*pItems)[i].lPosY = lPosY;

            m_nMaxX = std::max(m_nMaxX, (*pItems)[i].lPosX);
        }

        lPosY++;
    }

    updateGeometry();
}

Item ForecastWidget::getItem(const QString &key)
{

    QMapIterator<Board, QList<Item>*> it(m_items);
    QString project(key.split("-").first());

    while (it.hasNext()) {
        it.next();

        QList<Item>* pItems=it.value();

        if (pItems->first().project == project)
        {
            foreach(Item item, *pItems)
            {
                if (item.id == key)
                    return item;
            }

        }

    }

    return Item::Null;
}

void ForecastWidget::clear()
{
    m_items.clear();
    update();
}

QSize ForecastWidget::sizeHint() const
{
    return minimumSizeHint();
}

QSize ForecastWidget::minimumSizeHint() const
{
    return QSize(m_nMaxX * 220 * scale, 140 * m_items.values().size() * scale);
}

void ForecastWidget::paintEvent(QPaintEvent *)
{
    QRect rect(10, 10, 200, 120);
    QRect header(10, 10, 200, 20);
    QRect header2(30, 10, 200, 20);
    QRect body(15, 35, 200, 100);

    QPainter painter(this);
    painter.scale(scale, scale);

    {
        QMapIterator<Board, QList<Item>*> i(m_items);
        while (i.hasNext()) {
            i.next();

            QList<Item>* pItems=i.value();

            painter.setPen(Qt::SolidLine);

            if (pItems->size())
            {
            painter.drawText(QRectF(0, (rect.height()+2*rect.top()) * pItems->first().lPosY, 500, 20), Qt::AlignLeft, i.key().getName());
            }

            foreach(Item item, *pItems)
            {
                painter.save();

                painter.translate((rect.left() + rect.width()) * item.lPosX,
                                  (rect.height()+2*rect.top()) * item.lPosY +10);

                painter.setRenderHint(QPainter::Antialiasing, true);

                brush.setStyle(Qt::SolidPattern);

                {
                    painter.save();

                    // for the desired effect, no border will be drawn, and because a brush was set, the drawRoundRect will fill the box with the gradient brush.
                    painter.setPen(_outterborderPen);

                    painter.setBrush(brushGradient);
                    painter.translate(5, 5);
                    painter.drawRoundedRect(rect, 4, 4);

                    painter.restore();

                    if (item.belongsToBacklog())
                    {
                        brush.setColor(Qt::white);
                    }
                    else
                    {
                        brush.setColor(Qt::lightGray);
                    }
                    painter.setBrush(brush);
                    painter.drawRect(rect);
                }

                {
                    brush.setColor(Qt::green);

                    painter.setBrush(brush);
                    painter.drawRect(header);
                }

                painter.setPen(pen);
                painter.setBrush(brush);

                painter.drawText(header2, Qt::TextWordWrap + Qt::AlignLeft, item.id);


                if (m_issueTypes.contains(item.type))
                {
                    QPixmap icon;
                    icon.loadFromData(m_issueTypes[item.type]->icon);

                    painter.drawPixmap(header.left()+2, header.top()+2, 16, 16, icon);
                }

                painter.drawText(body,  Qt::TextWordWrap + Qt::AlignLeft, item.description);
                painter.restore();

            }
        }
    }

    {
        QStringList drawn;

        QMapIterator<Board, QList<Item>*> i(m_items);
        while (i.hasNext()) {
            i.next();

            QList<Item>* pItems=i.value();

            painter.setPen(Qt::SolidLine);

            foreach(Item item, *pItems)
            {
                if (item.dependingOn.size())
                {
                    foreach (QString link, item.dependingOn)
                    {
                        Item linked = getItem(link);

                        if (!drawn.contains(item.id + "->" + linked.id) &&
                                !drawn.contains(linked.id + "->" + item.id) &&
                                !linked.id.isEmpty() &&
                                linked.project != item.project)
                        {
                            int x1((rect.left() + rect.width()) * linked.lPosX + rect.width());
                            int y1((rect.height()+2*rect.top()) * linked.lPosY + rect.height());

                            int x2((rect.left() + rect.width()) * item.lPosX + rect.width());
                            int y2((rect.height()+2*rect.top()) * item.lPosY + rect.height());

                            painter.setPen(linkPen);

                            painter.drawLine(x1, y1, x2, y2);

                            drawn << item.id + "->" + linked.id;
                        }
                    }
                }

            }
        }
    }

    painter.setRenderHint(QPainter::Antialiasing, false);
    painter.setPen(palette().dark().color());
    painter.setBrush(Qt::NoBrush);


}

int ForecastWidget::getMaxActiveSprintSize() const
{
    QMapIterator<Board, int> it(m_boardActiveSprintSize);
    int nMax = 0;
    while (it.hasNext()) {
        it.next();
        nMax = std::max(nMax, it.value());
    }
    return nMax;
}

void ForecastWidget::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::MouseButton::LeftButton)
    {
        _pan = true;
        _panStartX = event->globalX();
        _panStartY = event->globalY();
        setCursor(Qt::ClosedHandCursor);
        event->accept();
        return;
    }
    event->ignore();
}

void ForecastWidget::mouseReleaseEvent(QMouseEvent *event)
{
    if (event->button() == Qt::MouseButton::LeftButton)
    {
        _pan = false;
        setCursor(Qt::ArrowCursor);
        event->accept();
        return;
    }
    event->ignore();
}

void ForecastWidget::mouseMoveEvent(QMouseEvent *event)
{
    if (_pan)
    {
        emit scroll( - (event->globalX() - _panStartX), - (event->globalY() - _panStartY));

        _panStartX = event->globalX();
        _panStartY = event->globalY();
        event->accept();
        return;
    }
    event->ignore();

}

void ForecastWidget::keyPressEvent(QKeyEvent *event)
{
    static const QList<int> zoomLevels = QList<int>()<<25<<33<<50<<67<<75<<90<<100<<110<<125<<150<<175<<200<<250<<300<<400<<500;

    if (event->matches(QKeySequence::ZoomIn))
    {
        foreach (int zoom, zoomLevels)
        {
            double val = map(zoom, 0, 100, 0, 1);

            if (val > scale)
            {
                scale = val;
                break;
            }
        }

        emit zoomChanged(scale);
        update();
        updateGeometry();
    }
    if (event->matches(QKeySequence::ZoomOut))
    {
        QList<int>::const_iterator it;
        for (it = zoomLevels.constEnd(); it != zoomLevels.constBegin(); it--)
        {
            double val = map(*it, 0, 100, 0, 1);

            if (val < scale)
            {
                scale = val;
                break;
            }
        }

        emit zoomChanged(scale);
        update();
        updateGeometry();
    }
}

void ForecastWidget::setIssueTypes(const QMap<int, JsonParser::IssueType *> &issueTypes)
{
    m_issueTypes = issueTypes;
}

bool Item::belongsToBacklog() const
{
    return sprint.isNull();
}
