#ifndef JSONPARSER_H
#define JSONPARSER_H

#include "board.h"

#include <QObject>

#include <QNetworkRequest>
#include <QNetworkReply>

#include <QJsonDocument>

#include "jiraissue.h"

class JiraSprint
{
public:
    int m_Id;
    QString m_State;
    QString m_Name;
};

class JsonParser : public QObject
{
    Q_OBJECT
public:
    explicit JsonParser(QString strServer,
                        QString strUser,
                        QString strPassword,
                        QString strProject);

    class IssueType
    {
    public:
        IssueType(long _id, QString _name)
            : id(_id)
            , name(_name)
        {

        }

        long id;
        QString name;
        QByteArray icon;
    };

    QMap<int, IssueType*> getIssueTypes();

    QList<Board> getBoards();
    QList<JiraIssue*> getBacklog(int board);
    QList<JiraSprint*> getSprints(int board);
    QList<JiraIssue*> getSprintIssues(int board, int sprint);

private:
    QString m_strServer;
    QString m_strUser;
    QString m_strPassword;
    QString m_strProject;


    void parseJSonDocumentForIssues(QJsonDocument document, QList<JiraIssue*> &rlstBacklog);
};

#endif // JSONPARSER_H
