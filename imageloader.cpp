#include "imageloader.h"

#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QSslConfiguration>

ImageLoader::ImageLoader(QByteArray& target, QObject *parent)
    : QObject(parent)
    , m_rTarget(target)
{

}

void ImageLoader::loadImage(QString user, QString pwd, QString url)
{
    QNetworkAccessManager *pNetManager = new QNetworkAccessManager(this);

    //Connect the finished Signal with the finished Function
    connect(pNetManager, SIGNAL(finished(QNetworkReply*)), this, SLOT(onImageReceived(QNetworkReply*)));

    //Request the Data from the URL
    QNetworkRequest oRequest(url);
    setAuthHeader(oRequest, user, pwd);

    QSslConfiguration conf = oRequest.sslConfiguration();
    conf.setPeerVerifyMode(QSslSocket::VerifyNone);
    oRequest.setSslConfiguration(conf);
    pNetManager->get(oRequest);
}

void ImageLoader::onImageReceived(QNetworkReply *pJsonFileContent)
{
    m_rTarget = pJsonFileContent->readAll();
    deleteLater();
}

void ImageLoader::setAuthHeader(QNetworkRequest &oRequest, QString strUsername, QString strPassword)
{
    QString cAuthString = QByteArray(strUsername.toLatin1() + ":" + strPassword.toLatin1()).toBase64();
    QByteArray oAutorizationString = ("Basic " + cAuthString).toLatin1();
    oRequest.setRawHeader("Authorization", oAutorizationString);
}
