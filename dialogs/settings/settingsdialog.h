#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include "board.h"

#include <QDialog>
#include <QMap>
#include <QString>

namespace Ui {
class SettingsDialog;
}

class SettingsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SettingsDialog(QWidget *parent = 0);
    ~SettingsDialog();

private slots:
    void save();

    void on_server_editingFinished();

private:
    Ui::SettingsDialog *ui;

    QList<Board> m_lstBoards;
    void loadBoards();
};

#endif // SETTINGSDIALOG_H
