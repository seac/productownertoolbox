#include "settingsdialog.h"
#include "ui_settingsdialog.h"

#include "settings.h"

#include "jsonparser.h"

void SettingsDialog::loadBoards()
{
    if (Settings::get().getUrl().isEmpty())
        return;

    m_lstBoards = JsonParser(Settings::get().getUrl(),
                             Settings::get().getUser(),
                             Settings::get().getPassword(),
                             QString::null).getBoards();

    QList<Board> boards(Settings::get().getBoards());

    ui->boards->clear();

    foreach(Board board, m_lstBoards)
    {
        QListWidgetItem* pItem = new QListWidgetItem(board.getName(), ui->boards);

        pItem->setFlags(pItem->flags() | Qt::ItemIsUserCheckable);

        auto itObj = std::find_if(
          boards.begin(), boards.end(),
          [board](Board o) { return o.getId() == board.getId(); }
        );
        if (itObj != boards.end())
        {
            pItem->setCheckState(Qt::Checked);
        }
        else
        {
            pItem->setCheckState(Qt::Unchecked);
        }
    }
}

SettingsDialog::SettingsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SettingsDialog)
{
    ui->setupUi(this);

    ui->server->setText(Settings::get().getUrl());
    ui->user->setText(Settings::get().getUser());
    ui->password->setText(Settings::get().getPassword());

    connect(this, SIGNAL(accepted()), this, SLOT(save()));

    loadBoards();
}

SettingsDialog::~SettingsDialog()
{
    delete ui;
}

void SettingsDialog::save()
{
    Settings::get().setUrl(ui->server->text());
    Settings::get().setUser(ui->user->text());
    Settings::get().setPassword(ui->password->text());

    QList<Board> boards;

    for(int i = 0; i < ui->boards->count(); ++i)
    {
        QListWidgetItem* item = ui->boards->item(i);

        if (item->checkState() == Qt::Checked)
        {
            boards << m_lstBoards[i];
        }
    }

    Settings::get().setBoards(boards);
}

void SettingsDialog::on_server_editingFinished()
{
    //loadBoards();
}
