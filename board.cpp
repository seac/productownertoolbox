#include "board.h"

Board::Board()
    : m_strName(QString::null)
    , m_nId(0)
{

}

Board::Board(const QString &name, const int &id)
    : m_strName(name),
      m_nId(id)
{

}

QString Board::getName() const
{
    return m_strName;
}

void Board::setName(const QString &name)
{
    m_strName = name;
}

int Board::getId() const
{
    return m_nId;
}

void Board::setId(int Id)
{
    m_nId = Id;
}
