#ifndef JIRERECEIVINGTHREAD_H
#define JIRERECEIVINGTHREAD_H

#include "board.h"

#include <QMap>
#include <QThread>


class JiraIssue;
class JiraSprint;

class GetIssuesFromJiraThread : public QThread
{
    Q_OBJECT
public:

    GetIssuesFromJiraThread(const QString& server,
                            const QString& user,
                            const QString& password,
                            const Board& board);

    QList<JiraIssue *> getIssues() const;

signals:
    void backlogReceived(Board board, QList<JiraIssue*>);
    void sprintsReceived(GetIssuesFromJiraThread* pReceiver, Board board, QList<JiraSprint*>);

protected:
    void run() override;

    QList<JiraIssue*> m_lstIssues;


    QString m_strServer;
    QString m_strUser;
    QString m_strPassword;

    Board m_board;
};

#endif // JIRERECEIVINGTHREAD_H
