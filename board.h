#ifndef BOARD_H
#define BOARD_H

#include <QString>

class Board
{
public:
    Board();
    Board(const QString& name,
          const int& id);

    QString getName() const;
    void setName(const QString &name);

    int getId() const;
    void setId(int Id);

private:
    QString m_strName;
    int m_nId;
};

inline bool operator< (const Board& lhs, const Board& rhs){ return lhs.getId() < rhs.getId(); }

#endif // BOARD_H
