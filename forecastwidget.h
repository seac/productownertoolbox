#ifndef JIRAITEM_H
#define JIRAITEM_H

#include <QPen>
#include <QWidget>
#include <QMap>

#include "board.h"

#include "jsonparser.h"

class Item
{
public:
    Item(Board _board, QString _project, QString _id, QString _description, QString _sprint, long _type, const QStringList& _dependencies = QStringList())
        : project(_project)
        , id(_id)
        , description(_description)
        , sprint(_sprint)
        , dependingOn(_dependencies)
        , board(_board)
        , type(_type)
    {
        project = _id.split("-").first();
    }

    bool belongsToBacklog() const;

    QString project;
    QString id;
    QString description;
    QString sprint;

    QStringList dependingOn;

    int lPosX;
    int lPosY;


    Board board;
    long type;

    static const Item Null;
};

class ForecastWidget : public QWidget
{
    Q_OBJECT
public:
    explicit ForecastWidget(QWidget *parent = 0);

    void addItem(const Item& item);

    Item getItem(const QString& key);

    void clear();

    void setIssueTypes(const QMap<int, JsonParser::IssueType *> &issueTypes);

signals:
    void scroll(int dx, int dy);
    void zoomChanged(qreal zoom);

public slots:

private:
    QSize sizeHint() const;
    QSize minimumSizeHint() const;
    void paintEvent(QPaintEvent *pEvent);
    int getMaxActiveSprintSize() const;

    QPen pen, _outterborderPen, linkPen;
    QBrush brush, brushGradient;
    bool antialiased;
    bool transformed;

    QLinearGradient gradient;

    QMap<Board, QList<Item>*> m_items;
    QMap<Board, int> m_boardActiveSprintSize;

    QMap<int, JsonParser::IssueType*> m_issueTypes;

    int m_nMaxX;

    // QWidget interface
protected:
    virtual void mousePressEvent(QMouseEvent *event) override;
    virtual void mouseReleaseEvent(QMouseEvent *event) override;
    virtual void mouseMoveEvent(QMouseEvent *event) override;
    virtual void keyPressEvent(QKeyEvent *event) override;

    bool _pan;
    int _panStartX, _panStartY;
    qreal scale;

    // QWidget interface
protected:
};

#endif // JIRAITEM_H
