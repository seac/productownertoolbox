#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "forecastwidget.h"
#include "dialogs/about/aboutdialog.h"
#include "getissuesfromjirathread.h"
#include "settings.h"
#include <dialogs/settings/settingsdialog.h>
#include "jsonparser.h"


#include <QLabel>
#include <QPaintEvent>
#include <QScrollBar>

#include <QFileDialog>
#include <QDebug>
#include "logging.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    qRegisterMetaType<QList<JiraSprint*>>("QList<JiraSprint*>");
    qRegisterMetaType<Board>("Board");
    qRegisterMetaType<QList<JiraIssue*>>("QList<JiraIssue*>");

    ui->setupUi(this);

    QMap<int, JsonParser::IssueType*> issueTypes = JsonParser(Settings::get().getUrl(),
                                                              Settings::get().getUser(),
                                                              Settings::get().getPassword(),
                                                              QString::null).getIssueTypes();

    m_pForecastWidget = new ForecastWidget();

    m_pForecastWidget->setIssueTypes(issueTypes);

    connect(m_pForecastWidget, &ForecastWidget::scroll, [&](int dx, int dy){
        ui->scrollArea->horizontalScrollBar()->setValue(ui->scrollArea->horizontalScrollBar()->value() + dx);
        ui->scrollArea->verticalScrollBar()->setValue(ui->scrollArea->verticalScrollBar()->value() + dy);
    });

    receiveBacklogs();

    ui->scrollArea->setBackgroundRole(QPalette::Dark);
    ui->scrollArea->setWidgetResizable(true);
    ui->scrollArea->setWidget(m_pForecastWidget);

    m_pForecastWidget->grabKeyboard();

    QLabel* pLabel = new QLabel();

    pLabel->setMinimumSize(pLabel->sizeHint());
    pLabel->setIndent(3);

    auto fnZoomLabelUpdate = [pLabel](qreal zoom)
    {
        pLabel->setText(QString("Zoom %1 %").arg(zoom * 100));
    };

    connect(m_pForecastWidget, &ForecastWidget::zoomChanged, fnZoomLabelUpdate);

    fnZoomLabelUpdate(1);

    ui->statusBar->addWidget(pLabel);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionAbout_triggered()
{
    AboutDialog(this).exec();
}

void MainWindow::on_action_Settings_triggered()
{
    SettingsDialog(this).exec();

    receiveBacklogs();
}

void MainWindow::sprintReceived(GetIssuesFromJiraThread*, Board board, QList<JiraSprint *> issues)
{
    qDebug() << QString("Received %3 sprints for %2(%1)")
                .arg(board.getId())
                .arg(board.getName())
                .arg(issues.size());

}

void MainWindow::backlogReceived(Board board, QList<JiraIssue *> issues)
{
    qDebug() << QString("Received %3 issues for %2(%1)")
                .arg(board.getId())
                .arg(board.getName())
                .arg(issues.size());

    foreach(JiraIssue* pIssue, issues)
    {
        m_pForecastWidget->addItem(Item(board, QString::null, pIssue->getIssueKey(), pIssue->getIssueSummary(), pIssue->getSprint() ? pIssue->getSprint()->m_Name : QString::null, pIssue->getIssueType(), pIssue->getLinks()));
    }

    m_pForecastWidget->update();
    ui->scrollArea->update();
}

void MainWindow::receiveBacklogs()
{
    m_pForecastWidget->clear();

    foreach (Board board, Settings::get().getBoards())
    {
        qDebug() << QString("Start to receive data for: %2(%1)").arg(board.getId()).arg(board.getName());

        GetIssuesFromJiraThread *pReceiver = new GetIssuesFromJiraThread(Settings::get().getUrl(),
                                                                         Settings::get().getUser(),
                                                                         Settings::get().getPassword(),
                                                                         board);

        connect(pReceiver, SIGNAL(sprintsReceived(GetIssuesFromJiraThread*,Board,QList<JiraSprint*>)), this,  SLOT(sprintReceived(GetIssuesFromJiraThread*,Board,QList<JiraSprint*>)));
        connect(pReceiver, SIGNAL(backlogReceived(Board,QList<JiraIssue*>)), this,  SLOT(backlogReceived(Board,QList<JiraIssue*>)));

        pReceiver->start();
    }
}

void MainWindow::on_actionRefresh_triggered()
{
    receiveBacklogs();
}

void MainWindow::on_actionExport_triggered()
{
    QString filename(QFileDialog::getSaveFileName(this, "Save image as...", QString::null, "Images (*.png *.xpm *.jpg)"));

    QPixmap pixMap = QPixmap::grabWidget(m_pForecastWidget);
    pixMap.save(filename);
}
