#include "getissuesfromjirathread.h"
#include "jsonparser.h"
#include "logging.h"

GetIssuesFromJiraThread::GetIssuesFromJiraThread(const QString &server, const QString &user, const QString &password, const Board &board)
    :
      m_strServer(server)
    , m_strUser(user)
    , m_strPassword(password)
    , m_board(board)
{}

void GetIssuesFromJiraThread::run()
{
    JsonParser parser(m_strServer, m_strUser, m_strPassword, QString::null);

    QList<JiraSprint*> lstSprints = parser.getSprints(m_board.getId());

    emit sprintsReceived(this, m_board, lstSprints);

    LOG(QString("Received %1 sprints for board %2(%3)").arg(lstSprints.size()).arg(m_board.getName()).arg(m_board.getId()));

    foreach(JiraSprint* pSprint, lstSprints)
    {
        QList<JiraIssue*> issues(parser.getSprintIssues(m_board.getId(), pSprint->m_Id));
        qDebug() << "number of issues after adding " << pSprint->m_Name << " is " << issues.size();

        m_lstIssues << issues;

        foreach(JiraIssue* pIssue, issues)
        {
            pIssue->setSprint(pSprint);
        }
    }

    m_lstIssues << parser.getBacklog(m_board.getId());

    emit backlogReceived(m_board, m_lstIssues);
}


QList<JiraIssue *> GetIssuesFromJiraThread::getIssues() const
{
    return m_lstIssues;
}
