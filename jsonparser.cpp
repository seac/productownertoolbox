#include "jsonparser.h"

#include <QJsonObject>
#include <QJsonArray>
#include "imageloader.h"

#include <QEventLoop>

#include <jira/jira.h>

#include "logging.h"

static const QString key_id("id");
static const QString key_key("key");
static const QString key_summary("summary");

JsonParser::JsonParser(QString strServer,
                       QString strUser,
                       QString strPassword,
                       QString strProject)
    : m_strServer(strServer)
    , m_strUser(strUser)
    , m_strPassword(strPassword)
    , m_strProject(strProject)
{
}

/*!
 * \brief JsonParser::getBoards
 * Retrieves all known boards
 * \return
 */
QList<Board> JsonParser::getBoards()
{
    QList<Board> lstBoards;

    Jira oJira(m_strServer, m_strUser, m_strPassword);
    QEventLoop oLoop(this);

    int nStartAt = 0;

    connect(&oJira, &Jira::onDataReceived, [&](QJsonDocument document)
    {
        bool bIsLast = document.object()["isLast"].toBool();
        QJsonArray jJsonArray = document.object()["values"].toArray();

        foreach (const QJsonValue & jJsonValue, jJsonArray)
        {
            QJsonObject jCurrentJsonObject = jJsonValue.toObject();
            lstBoards << Board(jCurrentJsonObject["name"].toString(), jCurrentJsonObject["id"].toInt());
        }

        if (!bIsLast)
        {
            int nMaxResults = document.object()["maxResults"].toInt();
            nStartAt += nMaxResults;

            oJira.get(QString("/rest/agile/1.0/board?type=scrum&startAt=%1").arg(nStartAt));
        }
        else
        {
            oLoop.quit();
        }
    });

    oJira.get(QString("/rest/agile/1.0/board?type=scrum&startAt=%1").arg(nStartAt));
    oLoop.exec();

    return lstBoards;
}

void JsonParser::parseJSonDocumentForIssues(QJsonDocument document, QList<JiraIssue*> &rlstBacklog)
{
    QJsonArray jJsonArray = document.object()["issues"].toArray();

    foreach (const QJsonValue & jJsonValue, jJsonArray)
    {
        QJsonObject jCurrentJsonObject = jJsonValue.toObject();
        QJsonObject jJsonFields = jCurrentJsonObject["fields"].toObject();
        QJsonObject oJsonIssueType = jJsonFields["issuetype"].toObject();

        QJsonArray links = jJsonFields["issuelinks"].toArray();

        QStringList strLinks;

        foreach(const QJsonValue& link, links)
        {
            {
                QString strLink (link.toObject()["inwardIssue"].toObject()["key"].toString());

                if (!strLink.isEmpty())
                {
                    strLinks << strLink;
                }
            }
            {
                QString strLink (link.toObject()["outwardIssue"].toObject()["key"].toString());

                if (!strLink.isEmpty())
                {
                    strLinks << strLink;
                }
            }
        }

        JiraIssue *oIssue = new JiraIssue(jCurrentJsonObject[key_id].toInt());
        oIssue->setIssueKey(jCurrentJsonObject[key_key].toString());
        oIssue->setIssueSummary(jJsonFields[key_summary].toString());
        oIssue->setParent(jJsonFields[key_summary].toObject()[key_key].toString());
        oIssue->setLinks(strLinks);
        oIssue->setIssueType(oJsonIssueType["id"].toString().toLong());

        rlstBacklog << oIssue;
    }
}

/*!
 * \brief JsonParser::getIssueTypes
 * Retrieves all known issuetypes
 * \return
 */
QMap<int, JsonParser::IssueType *> JsonParser::getIssueTypes()
{
    QMap<int, IssueType*> m_mapIssueTypes;

    Jira oJira(m_strServer, m_strUser, m_strPassword);
    QEventLoop oLoop(this);

    connect(&oJira, &Jira::onDataReceived, [&](QJsonDocument document)
    {
        QJsonArray jJsonArray = document.array();

        foreach (const QJsonValue & jJsonValue, jJsonArray)
        {
            QJsonObject jCurrentJsonObject = jJsonValue.toObject();
            IssueType *pType = new IssueType(jCurrentJsonObject["id"].toString().toLong(),
                    jCurrentJsonObject["name"].toString());


            ImageLoader *pImageLoader = new ImageLoader(pType->icon, parent());

            pImageLoader->loadImage(m_strUser, m_strPassword, jCurrentJsonObject["iconUrl"].toString());

            m_mapIssueTypes.insert(pType->id, pType);
        }

        oLoop.quit();
    });

    oJira.get("/rest/api/2/issuetype");
    oLoop.exec();

    return m_mapIssueTypes;
}

// /rest/agile/1.0/board/{boardId}/backlog
QList<JiraIssue *> JsonParser::getBacklog(int board)
{
    QList<JiraIssue*> lstBacklogForBoard;

    Jira oJira(m_strServer, m_strUser, m_strPassword);
    QEventLoop oLoop(this);

    connect(&oJira, &Jira::onDataReceived, [&](QJsonDocument document)
    {
        parseJSonDocumentForIssues(document, lstBacklogForBoard);
        oLoop.quit();
    });

    oJira.get(QString("/rest/agile/1.0/board/%1/backlog").arg(board));
    oLoop.exec();

    return lstBacklogForBoard;
}

/*!
 * \brief JsonParser::getSprints
 * Returns all active and future sprints of the given board
 * \param board
 * \return
 */
QList<JiraSprint *> JsonParser::getSprints(int board)
{
    QList<JiraSprint*> lstSprintsOfBoard;

    Jira oJira(m_strServer, m_strUser, m_strPassword);
    QEventLoop oLoop(this);

    connect(&oJira, &Jira::onDataReceived, [&](QJsonDocument document)
    {
        QJsonArray jJsonArray = document.object()["values"].toArray();

        foreach (const QJsonValue & jJsonValue, jJsonArray)
        {
            QJsonObject jCurrentJsonObject = jJsonValue.toObject();

            JiraSprint *pSprint = new JiraSprint;

            pSprint->m_Id=jCurrentJsonObject["id"].toInt();
            pSprint->m_Name=jCurrentJsonObject["name"].toString();
            pSprint->m_State=jCurrentJsonObject["state"].toString();

            lstSprintsOfBoard << pSprint;
        }
        oLoop.quit();
    });

    oJira.get(QString("/rest/agile/1.0/board/%1/sprint?state=active,future").arg(board));
    oLoop.exec();

    return lstSprintsOfBoard;
}

/*!
 * \brief JsonParser::getSprintIssues
 * Retrieves all issues that are in the given sprint of the given board
 * \param board
 * \param sprint
 * \return
 */
QList<JiraIssue *> JsonParser::getSprintIssues(int board, int sprint)
{
    QList<JiraIssue*> lstIssuesForSprint;

    Jira oJira(m_strServer, m_strUser, m_strPassword);
    QEventLoop oLoop(this);

    connect(&oJira, &Jira::onDataReceived, [&](QJsonDocument document)
    {
        parseJSonDocumentForIssues(document, lstIssuesForSprint);
        oLoop.quit();
    });

    oJira.get(QString("/rest/agile/1.0/board/%1/sprint/%2/issue").arg(board).arg(sprint));
    oLoop.exec();

    return lstIssuesForSprint;
}
