#ifndef JIRAISSUE_H
#define JIRAISSUE_H

#include <QString>
#include <QIcon>

class JiraSprint;

class JiraIssue
{
public:
    JiraIssue(int nId);
    JiraIssue();
    void setIssueKey(QString strKey) { m_strKey = strKey; }
    void setIssueSummary(QString strSummary) { m_strSummary = strSummary; }

    int getIssueId() {return m_nId; }
    QString getIssueKey() { return m_strKey; }
    QString getIssueSummary() { return m_strSummary; }

    QString getParent() const;
    void setParent(const QString &Parent);

    void setRank(int Rank);

    QStringList getLinks() const;
    void setLinks(const QStringList &links);

    long getIssueType() const;
    void setIssueType(const long &IssueTypeName);

    JiraSprint *getSprint() const;
    void setSprint(JiraSprint *Sprint);

private:
    int     m_nId;
    QString m_strKey;
    QString m_strSummary;
    QString m_strParent;

    QStringList m_links;

    long m_IssueType;

    JiraSprint* m_pSprint;
};

#endif // JIRAISSUE_H
