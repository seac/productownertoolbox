#-------------------------------------------------
#
# Project created by QtCreator 2017-02-02T21:09:50
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

greaterThan(QT_MAJOR_VERSION, 4): CONFIG += c++11

lessThan(QT_MAJOR_VERSION, 5): QMAKE_CXXFLAGS += -std=c++11

TARGET = PO_Toolbox
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    forecastwidget.cpp \
    dialogs/settings/settingsdialog.cpp \
    dialogs/about/aboutdialog.cpp \
    getissuesfromjirathread.cpp \
    jiraissue.cpp \
    jsonparser.cpp \
    settings.cpp \
    board.cpp \
    imageloader.cpp \
    jira/jira.cpp \
    logging.cpp

HEADERS  += mainwindow.h \
    forecastwidget.h \
    dialogs/settings/settingsdialog.h \
    dialogs/about/aboutdialog.h \
    getissuesfromjirathread.h \
    jiraissue.h \
    jsonparser.h \
    settings.h \
    board.h \
    imageloader.h \
    jira/jira.h \
    logging.h

FORMS    += mainwindow.ui \
    dialogs/settings/settingsdialog.ui \
    dialogs/about/aboutdialog.ui

RESOURCES += \
    resources/resources.qrc

DISTFILES += \
    bitbucket-pipelines.yml \
    PO_Toolbox.pro.user.626e9de \
    example_backlog.json
