#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "board.h"

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class JiraIssue;
class JiraSprint;
class ForecastWidget;
class GetIssuesFromJiraThread;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
private slots:
    void on_actionAbout_triggered();

    void on_action_Settings_triggered();

    void backlogReceived(Board board, QList<JiraIssue*> issues);
    void sprintReceived(GetIssuesFromJiraThread *pReceiver, Board board, QList<JiraSprint *> issues);

    void on_actionRefresh_triggered();

    void on_actionExport_triggered();

private:
    Ui::MainWindow *ui;
    ForecastWidget* m_pForecastWidget;
    void receiveBacklogs();
};

#endif // MAINWINDOW_H
