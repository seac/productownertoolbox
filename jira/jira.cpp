#include "jira/jira.h"

#include "logging.h"

#include <QJsonObject>
#include <QEventLoop>
#include <QNetworkAccessManager>

#include <QStandardPaths>
#include <QFile>


Jira::Jira(QString strServer, QString strUser, QString strPassword, QObject *parent)
    : QObject(parent)
    , m_strServer(strServer)
    , m_strUser(strUser)
    , m_strPassword(strPassword)
{

}

void Jira::get(const QString url)
{
    LOG("Jira::get(url=" + m_strServer + url + ")");

    QNetworkAccessManager *pNetworkAccessManager = new QNetworkAccessManager(this);

    connect(pNetworkAccessManager, &QNetworkAccessManager::finished, [&](QNetworkReply *pQNetworkReply)
    {
        QByteArray data(pQNetworkReply->readAll());

        emit onDataReceived(QJsonDocument::fromJson(data));

        pQNetworkReply->deleteLater();
    });

    QNetworkRequest oRequest(m_strServer + url);
    setAuthHeader(oRequest, m_strUser, m_strPassword);
    oRequest.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    QSslConfiguration conf = oRequest.sslConfiguration();
    conf.setPeerVerifyMode(QSslSocket::VerifyNone);
    oRequest.setSslConfiguration(conf);

    pNetworkAccessManager->get(oRequest);
}

void Jira::setAuthHeader(QNetworkRequest &oRequest, QString strUsername, QString strPassword)
{
    QString cAuthString = QByteArray(strUsername.toLatin1() + ":" + strPassword.toLatin1()).toBase64();
    QByteArray oAutorizationString = ("Basic " + cAuthString).toLatin1();
    oRequest.setRawHeader("Authorization", oAutorizationString);
}
