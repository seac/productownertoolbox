#ifndef JIRA_H
#define JIRA_H

#include <QJsonDocument>
#include <QNetworkReply>
#include <QObject>

class Jira : public QObject
{
    Q_OBJECT
public:
    explicit Jira(QString strServer,
                  QString strUser,
                  QString strPassword,
                  QObject *parent = nullptr);

    void get(const QString url);

signals:
    void onDataReceived(QJsonDocument);

private:
    void setAuthHeader(QNetworkRequest &oRequest, QString strUsername, QString strPassword);

    QString m_strServer;
    QString m_strUser;
    QString m_strPassword;
};

#endif // JIRA_H
