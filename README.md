This application is intended to help scrum teams that use jira to better spot relationships between different projects.

This tool will load the backlog of each board and draw it row by row,
adding lines that show the relationships.

To get started you will need Qt5, compilation should be straight forward without any fancy dependencies.

To use the tool open the settings and enter the credentials of for jira and the server adress - after that a restart is necessary - to load the scrum boards.
Once opened again enter the settings again and select which boards you want to see on one graph.
After that the tool should load the backlogs using jiras rest api and draw the plots.