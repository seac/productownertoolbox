#ifndef IMAGELOADER_H
#define IMAGELOADER_H

#include <QObject>

class QNetworkReply;
class QNetworkRequest;

class ImageLoader : public QObject
{
    Q_OBJECT
public:
    explicit ImageLoader(QByteArray& target, QObject *parent = 0);

    void loadImage(QString user, QString pwd, QString url);

signals:
public slots:
    void onImageReceived(QNetworkReply* pJsonFileContent);

private:
    void setAuthHeader(QNetworkRequest &oRequest, QString strUsername, QString strPassword);


    QByteArray& m_rTarget;
};

#endif // IMAGELOADER_H
