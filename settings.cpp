#include "settings.h"

#include <QSettings>
#include <QStringList>

QString g_strUser("User");
QString g_strPassword("Password");
QString g_strUrl("Url");
QString g_strBoards("Boards");

Settings::Settings()
    : m_pSettings(new QSettings("seac", "PO_Toolbox"))
{

}

Settings &Settings::get()
{
    static Settings* pMe = new Settings;

    return *pMe;
}

QString Settings::getUser() const
{
    return m_pSettings->value(g_strUser).toString();
}

QString Settings::getPassword() const
{
    return m_pSettings->value(g_strPassword).toString();
}

QString Settings::getUrl() const
{
    return m_pSettings->value(g_strUrl).toString();
}

QList<Board> Settings::getBoards() const
{
    QList<QVariant> items(m_pSettings->value(g_strBoards).toList());
    QList<Board> retval;

    foreach(QVariant item, items)
    {
        QStringList data = item.toString().split("=");
        retval << Board(data.last(), data.first().toInt());
    }

    return retval;
}

void Settings::setUser(QString data)
{
    m_pSettings->setValue(g_strUser, data);
}

void Settings::setPassword(QString data)
{
    m_pSettings->setValue(g_strPassword, data);
}

void Settings::setUrl(QString data)
{
    m_pSettings->setValue(g_strUrl, data);
}

void Settings::setBoards(QList<Board> data)
{
    QList<QVariant> items;

    foreach(Board board, data)
    {
        items << QString("%1=%2")
                 .arg(board.getId())
                 .arg(board.getName());
    }

    m_pSettings->setValue(g_strBoards, items);
}
